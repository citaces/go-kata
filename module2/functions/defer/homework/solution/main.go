package main

import (
	"errors"
	"fmt"
)

type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
)

func ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error) {
	job.Merged = make(map[string]string)
	var err error
	switch {
	case len(job.Dicts) > 1:
		for _, elem := range job.Dicts {
			if elem != nil {
				for idx, val := range elem {
					job.Merged[idx] = val
				}
			} else {
				err = errNilDict
				break
			}
		}
	default:
		err = errNotEnoughDicts
	}
	job.IsFinished = true
	return job, err
}

func main() {
	job, err := ExecuteMergeDictsJob(&MergeDictsJob{})
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("job: %+v, err: %v\n", job, err)
	}
	job, err = ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"}, nil}})
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("job: %+v, err: %v\n", job, err)
	}
	job, err = ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"}, {"b": "c"}}})
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("job: %+v, err: %v\n", job, err)
	}
}
