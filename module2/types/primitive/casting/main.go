package main

import "fmt"

func main() {
	var numberInt int
	var numberFloat float32 = 3
	numberInt = int(numberFloat)
	fmt.Printf("тип: %T, значение: %v\n", numberInt, numberInt)
	numberFloat = float32(numberInt)
	fmt.Printf("тип: %T, значение: %v\n", numberFloat, numberFloat)
	numberUint8 := uint8(numberFloat)
	fmt.Printf("тип: %T, значение: %v", numberUint8, numberUint8)
}
