package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func del(s *[]User) {
	for i, elem := range *s {
		if elem.Age > 40 && i < len(*s)-1 {
			*s = append((*s)[:i], (*s)[i+1:]...)
		} else if elem.Age > 40 && i == len(*s)-1 {
			*s = (*s)[:i]
		}
	}
}

func main() {
	users := []User{
		{
			Name: "Alice",
			Age:  21,
		},
		{
			Name: "John",
			Age:  34,
		},
		{
			Name: "Alexander",
			Age:  45,
		},
		{
			Name: "Ivan",
			Age:  13,
		},
		{
			Name: "Denis",
			Age:  44,
		},
		{
			Name: "Mary",
			Age:  26,
		},
		{
			Name: "Rose",
			Age:  41,
		},
	}
	del(&users)
	fmt.Println(users)
}
