package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func cut(s *[]User) {
	*s = (*s)[1:]
	*s = (*s)[:len(*s)-1]
}

func main() {
	users := []User{
		{
			Name: "Alice",
			Age:  21,
		},
		{
			Name: "John",
			Age:  34,
		},
		{
			Name: "Alexander",
			Age:  45,
		},
		{
			Name: "Ivan",
			Age:  13,
		},
		{
			Name: "Denis",
			Age:  44,
		},
		{
			Name: "Mary",
			Age:  26,
		},
		{
			Name: "Rose",
			Age:  41,
		},
	}
	cut(&users)
	fmt.Println(users)
}
