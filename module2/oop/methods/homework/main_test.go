package main

import "testing"

func Test_multiply(t *testing.T) {
	type args struct {
		a float64
		b float64
	}

	type testCases struct {
		name string
		args args
		want float64
	}

	tests := []testCases{
		{
			"should multiply correctly a,b when they are equal",
			args{2, 2},
			4,
		},
		{
			"should multiply correctly a is less then b",
			args{1, 2},
			2,
		},
		{
			"should multiply correctly a is less then b",
			args{1, 3},
			3,
		},
		{
			"should multiply correctly a is less then b",
			args{1, 4},
			4,
		},
		{
			"should multiply correctly a is less then b",
			args{1, 5},
			5,
		},
		{
			"should multiply correctly a is less then b",
			args{1, 6},
			6,
		},
		{
			"should multiply correctly a is less then b",
			args{1, 7},
			7,
		},
		{
			"should multiply correctly a is bigger then b",
			args{8, 1},
			8,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := multiply(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("multiply() = %.0f, want %.0f", got, tt.want)
			}
		})
	}
}

func Test_divide(t *testing.T) {
	type args struct {
		a float64
		b float64
	}

	type testCases struct {
		name string
		args args
		want float64
	}

	tests := []testCases{
		{
			"should divide correctly a,b when they are equal",
			args{2, 2},
			1,
		},
		{
			"should multiply correctly a is less then b",
			args{4, 8},
			0.5,
		},
		{
			"should multiply correctly a is bigger then b",
			args{10, 2},
			5,
		},
		{
			"should multiply correctly a is bigger then b",
			args{20, 2},
			10,
		},
		{
			"should multiply correctly a is bigger then b",
			args{60, 10},
			6,
		},
		{
			"should multiply correctly a is bigger then b",
			args{80, 40},
			2,
		},
		{
			"should divide correctly a,b when they are equal",
			args{100, 100},
			1,
		},
		{
			"should multiply correctly a is bigger then b",
			args{8, 1},
			8,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := divide(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("divide() = %.0f, want %.0f", got, tt.want)
			}
		})
	}
}

func Test_sum(t *testing.T) {
	type args struct {
		a float64
		b float64
	}
	type testCases struct {
		name string
		args args
		want float64
	}

	tests := []testCases{
		{
			"should divide correctly a,b when they are equal",
			args{2, 2},
			4,
		},
		{
			"should multiply correctly a is less then b",
			args{4, 8},
			12,
		},
		{
			"should multiply correctly a is bigger then b",
			args{10, 2},
			12,
		},
		{
			"should multiply correctly a is bigger then b",
			args{20, 2},
			22,
		},
		{
			"should multiply correctly a is bigger then b",
			args{60, 10},
			70,
		},
		{
			"should multiply correctly a is bigger then b",
			args{80, 40},
			120,
		},
		{
			"should divide correctly a,b when they are equal",
			args{100, 100},
			200,
		},
		{
			"should multiply correctly a is bigger then b",
			args{8, 1},
			9,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := sum(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("sum() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_average(t *testing.T) {
	type args struct {
		a float64
		b float64
	}
	type testCases struct {
		name string
		args args
		want float64
	}

	tests := []testCases{
		{
			"should divide correctly a,b when they are equal",
			args{2, 2},
			2,
		},
		{
			"should multiply correctly a is less then b",
			args{4, 8},
			6,
		},
		{
			"should multiply correctly a is bigger then b",
			args{10, 2},
			6,
		},
		{
			"should multiply correctly a is bigger then b",
			args{20, 2},
			11,
		},
		{
			"should multiply correctly a is bigger then b",
			args{60, 10},
			35,
		},
		{
			"should multiply correctly a is bigger then b",
			args{80, 40},
			60,
		},
		{
			"should divide correctly a,b when they are equal",
			args{100, 100},
			100,
		},
		{
			"should multiply correctly a is bigger then b",
			args{8, 1},
			4.5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := average(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("average() = %v, want %v", got, tt.want)
			}
		})
	}
}
