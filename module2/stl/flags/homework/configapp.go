package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
)

type Config struct {
	AppName    string `json:"app_name"`
	Production bool   `json:"production"`
}

func main() {
	flag.Parse() // читаем флаги
	conf := Config{}
	configFile := flag.String("conf", "", "module2/stl/flags/homework/config.json")
	if configFile == nil || len(*configFile) == 0 {
		flag.Usage()
		os.Exit(-1)
	}
	data, err := os.ReadFile(*configFile)
	if err != nil {
		log.Fatal(err)
	}
	err = json.Unmarshal(data, &conf)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Production: %v\nAppName: %v", conf.Production, conf.AppName)
}

/*
1) go build configapp.go
2) ./configapp -conf ./config.json
*/
