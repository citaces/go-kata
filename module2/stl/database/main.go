package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	database, _ := sql.Open("sqlite3", "module2/stl/database/gopher.db")
	statement, _ := database.Prepare("CREATE TABLE IF NOT EXISTS people (id INTEGER PRIMARY KEY, firstname TEXT, lastname TEXT)")
	_, err := statement.Exec()
	if err != nil {
		log.Fatal(err)
	}
	statement, _ = database.Prepare("INSERT INTO people (firstname, lastname) VALUES (?,?)")
	_, err = statement.Exec("Lorem", "Ipsum")
	if err != nil {
		log.Fatal(err)
	}
	rows, _ := database.Query("SELECT id, firstname, lastname FROM people")
	var id int
	var firstname string
	var lastname string
	for rows.Next() {
		err = rows.Scan(&id, &firstname, &lastname)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("%d: %s, %s\n", id, firstname, lastname)
	}
}

/*
1: John, Doe
2: Lorem, Ipsum
3: Lorem, Ipsum
*/
