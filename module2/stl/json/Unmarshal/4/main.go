package main

import (
	"encoding/json"
	"fmt"
)

type Profile struct {
	Username  string
	Followers int
}
type Student struct {
	FirstName      string
	HeightInMeters float64
	IsMale         bool
	Languages      [2]string
	Subjects       []string
	Grades         map[string]string
	Profile        *Profile
}

func main() {

	data := []byte(`
	{
		"FirstName": "John",
		"HeightInMeters": 1.75,
		"IsMale": null,
		"Languages": [ "English" ],
		"Subjects": [ "Math", "Science" ],
		"Grades": null,
		"Profile": { "Followers": 1975 }
	}`)

	var john Student = Student{
		IsMale:    true,
		Languages: [2]string{"Korean", "Chinese"},
		Subjects:  nil,
		Grades:    map[string]string{"Math": "A"},
		Profile:   &Profile{Username: "johndoe91"},
	}

	fmt.Printf("Error: %v\n\n", json.Unmarshal(data, &john))

	fmt.Printf("%#v\n\n", john)
	fmt.Printf("%#v\n", john.Profile)
}

/*
Error: <nil>
main.Student{FirstName:"John", HeightInMeters:1.75, IsMale:true, Languages:[2]string{"English", ""}, Subjects:[]string{"Math", "Science"}, Grades:map[string]string(nil), Profile:(*main.Profile)(0x14000114018)}
&main.Profile{Username:"johndoe91", Followers:1975}
*/
