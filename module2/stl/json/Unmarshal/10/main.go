package main

import (
	"bytes"
	"encoding/json"
	"fmt"
)

type Person struct {
	Name string
	Age  int
}

func main() {

	buf := new(bytes.Buffer)
	bufEncoder := json.NewEncoder(buf)
	err := bufEncoder.Encode(Person{"Ross Geller", 28})
	if err != nil {
		panic(err)
	}
	err = bufEncoder.Encode(Person{"Monica Geller", 27})
	if err != nil {
		panic(err)
	}
	err = bufEncoder.Encode(Person{"Jack Geller", 56})
	if err != nil {
		panic(err)
	}
	fmt.Println(buf) // calls `buf.String()` method
}
