package main

import (
	"encoding/json"
	"fmt"
)

type Profile struct {
	Username  string `json:"uname"`
	Followers int    `json:"f_count"`
}

type Student struct {
	FirstName      string   `json:"fname"`
	LastName       string   `json:"-"` // discard
	HeightInMeters float64  `json:"height"`
	IsMale         bool     `json:"male"`
	Languages      []string `json:",omitempty"`
	Profile        Profile  `json:"profile"`
}

func main() {

	data := []byte(`
	{
		"fname": "John",
		"LastName": "Doe",
		"height": 1.75,
		"IsMale": true,
		"Languages": null,
		"profile": {
			"uname": "johndoe91",
			"Followers": 1975
		}
	}`)
	var john Student = Student{
		Languages: []string{"English", "French"},
	}

	fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))

	fmt.Printf("%#v\n", john)
}

/*
Error: <nil>
main.Student{FirstName:"John", LastName:"", HeightInMeters:1.75, IsMale:false, Languages:[]string(nil), Profile:main.Profile{Username:"johndoe91", Followers:0}}
*/
