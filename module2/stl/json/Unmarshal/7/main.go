package main

import (
	"encoding/json"
	"fmt"
)

// Student declares `Student` map
type Student map[string]interface{}

func main() {

	data := []byte(`
	{
		"id": 123,
		"fname": "John",
		"height": 1.75,
		"male": true,
		"languages": null,
		"subjects": [ "Math", "Science" ],
		"profile": {
			"uname": "johndoe91",
			"f_count": 1975
		}
	}`)

	var john Student

	fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))

	fmt.Printf("%#v\n\n", john)

	i := 1
	for k, v := range john {
		fmt.Printf("%d: key (`%T`)`%v`, value (`%T`)`%#v`\n", i, k, k, v, v)
		i++
	}
}

/*
1: key (`string`)`fname`, value (`string`)`"John"`
2: key (`string`)`height`, value (`float64`)`1.75`
3: key (`string`)`male`, value (`bool`)`true`
4: key (`string`)`languages`, value (`<nil>`)`<nil>`
5: key (`string`)`subjects`, value (`[]interface {}`)`[]interface {}{"Math", "Science"}`
6: key (`string`)`profile`, value (`map[string]interface {}`)`map[string]interface {}{"f_count":1975, "uname":"johndoe91"}`
7: key (`string`)`id`, value (`float64`)`123`
*/
