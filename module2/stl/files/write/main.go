package main

import (
	"fmt"
	"os"
	"strings"
	"unicode"
)

var T = map[string]string{
	"а": "a",
	"б": "b",
	"в": "v",
	"г": "g",
	"д": "d",
	"е": "e",
	"ё": "e",
	"ж": "zh",
	"з": "z",
	"и": "i",
	"й": "y",
	"к": "k",
	"л": "l",
	"м": "m",
	"н": "n",
	"о": "o",
	"п": "p",
	"р": "r",
	"с": "s",
	"т": "t",
	"у": "u",
	"ф": "f",
	"х": "kh",
	"ц": "ts",
	"ч": "4",
	"ш": "sh",
	"щ": "sh'",
	"ы": "y",
	"ь": "'",
	"э": "e",
	"ю": "u",
	"я": "ya",
}

func translit(rs []rune) []rune {
	var latin strings.Builder
	for _, elem := range rs {
		switch unicode.IsUpper(elem) {
		case true:
			if val, ok := T[strings.ToLower(string(elem))]; ok {
				latin.WriteString(strings.ToUpper(val))
			} else {
				latin.WriteString(string(elem))
			}
		case false:
			if val, ok := T[string(elem)]; ok {
				latin.WriteString(val)
			} else {
				latin.WriteString(string(elem))
			}
		}
	}
	r := []rune(latin.String())
	return r
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	reader, err := os.ReadFile("module2/stl/files/write/example.txt")
	check(err)
	x := translit([]rune(string(reader)))
	s := strings.Split(string(x), " ")
	o, err := os.Create("module2/stl/files/write/example.processed.txt")
	check(err)
	defer o.Close()
	for _, elem := range s {
		fmt.Print("Enter your name: ")
		fmt.Println(elem)
		fmt.Printf("Hello %s\n", elem)
		_, err = o.WriteString(elem + " ")
		check(err)
	}
}
