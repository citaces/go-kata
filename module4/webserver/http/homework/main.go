package main

import (
	"gitlab.com/citaces/go-kata/module4/webserver/http/homework/handlers"
	"gitlab.com/citaces/go-kata/module4/webserver/http/homework/repository"
	"gitlab.com/citaces/go-kata/module4/webserver/http/homework/service"
)

func main() {
	r := repository.NewUsersRepository()
	s := service.NewUsersService(r)
	h := handlers.NewUsersHandlers(s)
	h.Router()
}
