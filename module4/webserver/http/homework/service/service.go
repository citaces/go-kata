package service

import (
	"gitlab.com/citaces/go-kata/module4/webserver/http/homework/model"
	"gitlab.com/citaces/go-kata/module4/webserver/http/homework/repository"
)

type Service interface {
	ListUsers() []model.User
	ListUser(id string) (model.User, error)
	AddUser(user model.User)
}

type UsersService struct {
	repository repository.Repository
}

func NewUsersService(repository repository.Repository) *UsersService {
	return &UsersService{repository}
}

func (u *UsersService) ListUsers() []model.User {
	return u.repository.GetUsers()
}

func (u *UsersService) ListUser(id string) (model.User, error) {
	return u.repository.GetUser(id)
}

func (u *UsersService) AddUser(user model.User) {
	u.repository.CreateUser(user)
}
