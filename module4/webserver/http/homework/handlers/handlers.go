package handlers

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"

	"gitlab.com/citaces/go-kata/module4/webserver/http/homework/model"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"gitlab.com/citaces/go-kata/module4/webserver/http/homework/service"
)

type Handlers struct {
	service service.Service
}

func NewUsersHandlers(service service.Service) *Handlers {
	return &Handlers{service}
}

func (h *Handlers) Router() {
	r := chi.NewRouter()
	port := ":8080"

	r.Use(middleware.Logger)

	r.Get("/", h.Home)
	r.Get("/users", h.Users)
	r.Get("/users/{id}", h.User)
	r.Post("/users", h.AddUser)
	r.Get("/upload", h.Upload)
	r.Post("/upload", h.UploadFile)
	r.Get("/public/{filename}", h.PublicFile)

	srv := &http.Server{
		Addr:    port,
		Handler: r,
	}

	go func() {
		fmt.Println("server started")
		err := srv.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			log.Fatal(err)
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	<-quit

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	err := srv.Shutdown(ctx)
	if err != nil {
		log.Fatal("Server Shutdown:", err)
	}
	log.Println("Server exiting")
}

func (h *Handlers) Home(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	hw := "Hello, this is HOMEPAGE"
	err := json.NewEncoder(w).Encode(map[string]string{"message": hw})
	if err != nil {
		log.Fatal(err)
	}
}

func (h *Handlers) Users(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	users := h.service.ListUsers()
	err := json.NewEncoder(w).Encode(users)
	if err != nil {
		log.Fatal(err)
	}
}

func (h *Handlers) User(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	id := chi.URLParam(r, "id")
	user, err := h.service.ListUser(id)
	if err != nil {
		http.NotFound(w, r)
		return
	}
	err = json.NewEncoder(w).Encode(user)
	if err != nil {
		log.Fatal(err)
	}
}

func (h *Handlers) AddUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	user := model.User{}
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		log.Fatal(err)
	}
	h.service.AddUser(user)
	users := h.service.ListUsers()
	err = json.NewEncoder(w).Encode(users)
	if err != nil {
		log.Fatal(err)
	}
}

func (h *Handlers) Upload(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "public/upload.html")
}

func (h *Handlers) UploadFile(w http.ResponseWriter, r *http.Request) {
	file, handler, err := r.FormFile("file")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	f, err := os.OpenFile(filepath.Join("module4/webserver/http/homework/public", handler.Filename), os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	_, err = io.Copy(f, file)
	if err != nil {
		log.Fatal(err)
	}
}

func (h *Handlers) PublicFile(w http.ResponseWriter, r *http.Request) {
	filename := chi.URLParam(r, "filename")
	http.ServeFile(w, r, filepath.Join("module4/webserver/http/homework/public", filename))
}
