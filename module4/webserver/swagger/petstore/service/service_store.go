package service

import (
	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"
	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/repository"
)

type StoreService interface {
	StoreCreate(store model.Store) model.Store
	StoreDelId(id int) ([]model.Store, error)
	StoreGetById(id int) (model.Store, error)
	StoreGetByStatus() map[string]int
}

type StoreServ struct {
	repo repository.StoreStorager
}

func NewStoreServ(repository repository.StoreStorager) *StoreServ {
	return &StoreServ{repository}
}

func (s *StoreServ) StoreCreate(store model.Store) model.Store {
	return s.repo.Create(store)
}

func (s *StoreServ) StoreDelId(id int) ([]model.Store, error) {
	return s.repo.Del(id)
}

func (s *StoreServ) StoreGetById(id int) (model.Store, error) {
	return s.repo.GetByID(id)
}

func (s *StoreServ) StoreGetByStatus() map[string]int {
	return s.repo.GetByStatus()
}
