package controllers

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strconv"

	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/service"

	"github.com/go-chi/chi"
	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"
)

type PetController interface {
	PetCreate(w http.ResponseWriter, r *http.Request)
	PetUpdate(w http.ResponseWriter, r *http.Request)
	PetGetByID(w http.ResponseWriter, r *http.Request)
	PetFindByStatus(w http.ResponseWriter, r *http.Request)
	PetList(w http.ResponseWriter, r *http.Request)
	PetDel(w http.ResponseWriter, r *http.Request)
	PetUploadImg(w http.ResponseWriter, r *http.Request)
	PetUpdId(w http.ResponseWriter, r *http.Request)
}

type PetControl struct { // Pet контроллер
	service service.PetService
}

func NewPetControl(service service.PetService) *PetControl { // конструктор нашего контроллера
	return &PetControl{service}
}

func (p *PetControl) PetCreate(w http.ResponseWriter, r *http.Request) {
	var pet model.Pet
	err := json.NewDecoder(r.Body).Decode(&pet) // считываем приходящий json из *http.Request в структуру Pet

	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	pet = p.service.PetNew(pet) // создаем запись в нашем storage

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(pet)                             // записываем результат Pet json в http.ResponseWriter

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *PetControl) PetUpdate(w http.ResponseWriter, r *http.Request) {

	id := chi.URLParam(r, "petID")
	n, _ := strconv.Atoi(id)

	var pet model.Pet

	err := json.NewDecoder(r.Body).Decode(&pet) // считываем приходящий json из *http.Request в структуру Pet

	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
	pet.ID = n
	pet, err = p.service.PetUpdate(pet) // создаем запись в нашем storage
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(pet)                             // записываем результат Pet json в http.ResponseWriter

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *PetControl) PetGetByID(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		pet      model.Pet
		err      error
		petIDRaw string
		petID    int
	)

	petIDRaw = chi.URLParam(r, "petID") // получаем petID из chi router

	petID, err = strconv.Atoi(petIDRaw) // конвертируем в int
	if err != nil {                     // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	pet, err = p.service.PetGetByID(petID) // пытаемся получить Pet по id
	if err != nil {                        // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(pet)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *PetControl) PetFindByStatus(w http.ResponseWriter, r *http.Request) {
	var s []string
	for i, e := range r.URL.Query() {
		fmt.Println(i, e)
		if i == "status" {
			s = e
		}
	}
	pets := p.service.PetGetByStatus(s)
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err := json.NewEncoder(w).Encode(pets)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (p *PetControl) PetList(w http.ResponseWriter, r *http.Request) {
	pets := p.service.PetListAll()
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err := json.NewEncoder(w).Encode(pets)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (p *PetControl) PetDel(w http.ResponseWriter, r *http.Request) {
	i := chi.URLParam(r, "petID")
	id, err := strconv.Atoi(i)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	pets, err := p.service.PetDelId(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(pets)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (p *PetControl) PetUploadImg(w http.ResponseWriter, r *http.Request) {
	i := chi.URLParam(r, "petID")
	id, err := strconv.Atoi(i)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = r.ParseMultipartForm(32 << 20)
	if err != nil {
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	}

	file, handler, err := r.FormFile("file")
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer file.Close()

	f, err := os.CreateTemp("module4/webserver/swagger/petstore/photos/", ".*"+filepath.Ext(handler.Filename))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer f.Close()

	_, _ = io.Copy(f, file)

	pet := p.service.PetUpdImg(id, filepath.Base(f.Name()))

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(pet)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (p *PetControl) PetUpdId(w http.ResponseWriter, r *http.Request) {
	i := chi.URLParam(r, "petID")
	id, err := strconv.Atoi(i)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	name := r.FormValue("name")
	status := r.FormValue("status")
	pet := p.service.PetUpdById(id, name, status)
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(pet)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
