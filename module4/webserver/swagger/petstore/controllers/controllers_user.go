package controllers

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"
	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/service"
)

type UserController interface {
	UserCreate(w http.ResponseWriter, r *http.Request)
	UserCreateWithList(w http.ResponseWriter, r *http.Request)
	UserGetByUserName(w http.ResponseWriter, r *http.Request)
	UserUpdByUserName(w http.ResponseWriter, r *http.Request)
	UserDelByUserName(w http.ResponseWriter, r *http.Request)
}

type UserControl struct {
	service service.UserService
}

func NewUserControl(service service.UserService) *UserControl {
	return &UserControl{service}
}

func (u *UserControl) UserCreate(w http.ResponseWriter, r *http.Request) {
	var user model.User
	err := json.NewDecoder(r.Body).Decode(&user)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	user, err = u.service.UserCreate(user)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(user)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (u *UserControl) UserCreateWithList(w http.ResponseWriter, r *http.Request) {
	var users []model.User
	err := json.NewDecoder(r.Body).Decode(&users)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	users, err = u.service.UserCreateWithList(users)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(users)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (u *UserControl) UserGetByUserName(w http.ResponseWriter, r *http.Request) {
	un := chi.URLParam(r, "username")
	user, err := u.service.UserGetByUsername(un)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (u *UserControl) UserUpdByUserName(w http.ResponseWriter, r *http.Request) {
	un := chi.URLParam(r, "username")
	var user model.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	user, err = u.service.UserUpdByUserName(un, user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (u *UserControl) UserDelByUserName(w http.ResponseWriter, r *http.Request) {
	un := chi.URLParam(r, "username")
	user, err := u.service.UserDelByUserName(un)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
