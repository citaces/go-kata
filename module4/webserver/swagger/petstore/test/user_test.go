package test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"
	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/repository"
)

var (
	User1 = model.User{
		ID:         1,
		Username:   "durrrr",
		FirstName:  "Tom",
		LastName:   "Dwan",
		Email:      "test@abc.com",
		Password:   "qwerty",
		Phone:      "88005553535",
		UserStatus: 0,
	}
	User2 = model.User{
		ID:         2,
		Username:   "kidpoker",
		FirstName:  "Daniel",
		LastName:   "Negrianu",
		Email:      "tes@abc.com",
		Password:   "qwerty",
		Phone:      "123456",
		UserStatus: 0,
	}
)

func TestUserStorage_Create(t *testing.T) {
	repo := repository.NewUserStorage()
	user, err := repo.Create(User1)
	assert.NoError(t, err, "create test")
	assert.Equal(t, user, User1, "create test")
}

func TestUserStorage_GetByUsername(t *testing.T) {
	repo := repository.NewUserStorage()
	user, _ := repo.Create(User1)
	pid, err := repo.GetByUsername("boss")
	assert.Error(t, err, "getByUsername test")
	assert.Equal(t, model.User{}, pid)
	pid, err = repo.GetByUsername("durrrr")
	assert.NoError(t, err, "getByUsername test")
	assert.Equal(t, user, pid)
}

func TestUserStorage_Del(t *testing.T) {
	repo := repository.NewUserStorage()
	_, _ = repo.Create(User1)
	_, _ = repo.Create(User2)
	_, err := repo.UserDelByUserName("kidpoker")
	assert.NoError(t, err, "test Del")
	assert.Equal(t, 1, len(repo.Data))
	sl, err := repo.UserDelByUserName("qweqweqwe")
	assert.Error(t, err, "test Del")
	assert.Equal(t, []model.User{}, sl)
}

func TestUserStorage_Update(t *testing.T) {
	repo := repository.NewUserStorage()
	user, _ := repo.Create(User1)
	user.LastName = "IVANOV"
	pid, err := repo.UserUpdByUserName("durrrr", user)
	assert.NoError(t, err, "Update test")
	assert.Equal(t, user.LastName, pid.LastName)
	user.LastName = "IVANOV"
	pid, err = repo.UserUpdByUserName("BOSS", user)
	assert.Error(t, err, "Update test")
}
