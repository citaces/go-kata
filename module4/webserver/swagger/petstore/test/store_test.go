package test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"
	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/repository"
)

var (
	Store1 = model.Store{
		ID:       1,
		PetID:    1,
		Quantity: 1,
		ShipDate: "2023-05-04T06:07:59.460Z",
		Status:   "sold",
		Complete: true,
	}
	Store2 = model.Store{
		ID:       2,
		PetID:    2,
		Quantity: 2,
		ShipDate: "2022-05-04T06:07:59.460Z",
		Status:   "sold",
		Complete: false,
	}
)

func TestStoreStorage_Create(t *testing.T) {
	repo := repository.NewStoreStorage()
	store := repo.Create(Store1)
	assert.Equal(t, store, Store1, "create test")
}

func TestStoreStorage_GetByID(t *testing.T) {
	repo := repository.NewStoreStorage()
	store := repo.Create(Store1)
	pid, err := repo.GetByID(100)
	assert.Error(t, err, "GetByID test")
	assert.Equal(t, model.Store{}, pid)
	pid, err = repo.GetByID(1)
	assert.NoError(t, err, "GetByID test")
	assert.Equal(t, store, pid)
}

func TestStoreStorage_GetByStatus(t *testing.T) {
	repo := repository.NewStoreStorage()
	repo.Create(Store1)
	repo.Create(Store2)
	sl := repo.GetByStatus()
	assert.Equal(t, 2, sl["sold"], "GetByStatus test")
}

func TestStoreStorage_Del(t *testing.T) {
	repo := repository.NewStoreStorage()
	repo.Create(Store1)
	repo.Create(Store2)
	_, err := repo.Del(2)
	assert.NoError(t, err, "test Del")
	assert.Equal(t, 1, len(repo.Data))
	sl, err := repo.Del(123)
	assert.Error(t, err, "test Del")
	assert.Equal(t, []model.Store{}, sl)
}
