package public

import (
	"io"

	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"
)

// swagger:route POST /pet pet petAddRequest
// Add a new pet to the store.
// responses:
//   200: petAddResponse

// swagger:parameters petAddRequest
//
//nolint:all
type petAddRequest struct {
	// Pet object that needs to be added to the store
	//
	// required:true
	// in:body
	Body model.Pet
}

// swagger:response petAddResponse
//
//nolint:all
type petAddResponse struct {
	// in:body
	Body model.Pet
}

// swagger:route POST /pet/{id} pet petUpdIdRequest
// Updates a pet in the store with form data.
// responses:
//   200: petUpdIdResponse

// swagger:parameters petUpdIdRequest
//
//nolint:all
type petUpdIdRequest struct {
	// ID of pet that needs to be updated
	//
	// required:true
	// in:path
	ID int `json:"id"`
	// ID of pet that needs to be updated
	//
	// required:false
	// in:formData
	Name string `json:"name"`
	// ID of pet that needs to be updated
	//
	// required:false
	// in:formData
	Status string `json:"status"`
}

// swagger:response petUpdIdResponse
//
//nolint:all
type petUpdIdResponse struct {
	// in:body
	Body model.Pet
}

// swagger:route POST /pet/{id}/uploadImage pet uploadPetPhotoRequest
// Uploads an image.
//
// Responses:
//   200:

// swagger:parameters uploadPetPhotoRequest
//
//nolint:all
type uploadPetPhotoRequest struct {
	// The ID of the pet to upload a photo for
	//
	// in: path
	// required: true
	ID int `json:"id"`

	// The file to upload
	//
	// in:formData
	// required: true
	// swagger:file
	File *io.ReadCloser `json:"file"`
}
