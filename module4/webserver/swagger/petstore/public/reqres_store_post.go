package public

import "gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"

// swagger:route POST /store/order store storePlaceAnOrderRequest
// Place an order for a pet.
// responses:
// 200: storePlaceAnOrderResponse

// swagger:parameters storePlaceAnOrderRequest
//
//nolint:all
type storePlaceAnOrderRequest struct {
	//order placed for purchasing the pet
	//required:true
	//in:body
	Body model.Store
}

// swagger:response storePlaceAnOrderResponse
//
//nolint:all
type storePlaceAnOrderResponse struct {
	//in:body
	Body model.Store
}
