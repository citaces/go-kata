package public

import "gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"

// swagger:route GET /store/order/{id} store storeGetByIDRequest
// Find purchase order by ID.
// responses:
// 200: storeGetByIDResponse

// swagger:parameters storeGetByIDRequest
//
//nolint:all
type storeGetByIDRequest struct {
	//required:true
	//in:path
	ID int `json:"id"`
}

// swagger:response storeGetByIDResponse
//
//nolint:all
type storeGetByIDResponse struct {
	//in:body
	Body model.Store
}

//swagger:route GET /store/inventory store storeGetByStatusRequest
// Returns pet inventories by status.
// Returns a map of status codes to quantities
// responses:
// 200: storeGetByStatusResponse

// swagger:parameters storeGetByStatusRequest

// swagger:response storeGetByStatusResponse
//
//nolint:all
type storeGetByStatusResponse struct {
	//in:body
}
