package public

import "gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"

// swagger:route DELETE /store/order/{id} store storeDeleteByIdRequest
// Delete purchase order by ID.
// responses:
// 200: storeDeleteByIdResponse

// swagger:parameters storeDeleteByIdRequest
//
//nolint:all
type storeDeleteByIdRequest struct {
	// For valid response try integer IDs with positive integer value. Negative or non-integer values will generate API errors
	//
	// ID of the order that needs to be deleted
	//required:true
	//in:path
	OrderID int `json:"id"`
}

// swagger:response storeDeleteByIdResponse
//
//nolint:all
type storeDeleteByIdResponse struct {
	//in:body
	Body []model.Store
}
