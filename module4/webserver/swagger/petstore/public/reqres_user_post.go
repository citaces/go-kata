package public

import "gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"

// swagger:route POST /user/createWithList user userCreateWithListRequest
// Creates list of users with given input array.
// responses:
// 200: userCreateWithListResponse

// swagger:parameters userCreateWithListRequest
//
//nolint:all
type userCreateWithListRequest struct {
	//List of user object
	//required:true
	//in:body
	Body []model.User
}

// swagger:response userCreateWithListResponse
//
//nolint:all
type userCreateWithListResponse struct {
	//in:body
	Body []model.User
}

// swagger:route POST /user user userCreateRequest
// Create user.
// responses:
// 200: userCreateResponse

// swagger:parameters userCreateRequest
//
//nolint:all
type userCreateRequest struct {
	//Created user object
	//required:true
	//in:body
	Body model.User
}

// swagger:response userCreateResponse
//
//nolint:all
type userCreateResponse struct {
	//in:body
	Body model.User
}
