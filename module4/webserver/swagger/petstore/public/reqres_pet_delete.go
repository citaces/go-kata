package public

import "gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"

// swagger:route DELETE /pet/{id} pet petDelRequest
// Del a pet from the store.
// responses:
//   200: petDelResponse

// swagger:parameters petDelRequest
//
//nolint:all
type petDelRequest struct {
	//
	// required:false
	// in:header
	API string `json:"api_key"`
	//
	// required:true
	// in:path
	ID int `json:"id"`
}

// swagger:response petDelResponse
//
//nolint:all
type petDelResponse struct {
	//in:body
	Body model.Pet
}
