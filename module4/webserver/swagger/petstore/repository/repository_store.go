package repository

import (
	"errors"
	"fmt"
	"sync"

	"gitlab.com/citaces/go-kata/module4/webserver/swagger/petstore/model"
)

type StoreStorager interface {
	Create(store model.Store) model.Store
	Del(id int) ([]model.Store, error)
	GetByID(storeID int) (model.Store, error)
	GetByStatus() map[string]int
}

type StoreStorage struct {
	Data               []*model.Store
	primaryKeyIDx      map[int]*model.Store
	autoIncrementCount int
	sync.Mutex
}

func NewStoreStorage() *StoreStorage {
	return &StoreStorage{
		Data:               make([]*model.Store, 0, 13),
		primaryKeyIDx:      make(map[int]*model.Store, 13),
		autoIncrementCount: 1,
	}
}

func (s *StoreStorage) Create(store model.Store) model.Store {
	s.Lock()
	defer s.Unlock()
	store.ID = s.autoIncrementCount
	s.primaryKeyIDx[store.ID] = &store
	s.autoIncrementCount++
	s.Data = append(s.Data, &store)
	return store
}

func (s *StoreStorage) GetByID(storeID int) (model.Store, error) {
	s.Lock()
	defer s.Unlock()
	if v, ok := s.primaryKeyIDx[storeID]; ok {
		return *v, nil
	}
	return model.Store{}, fmt.Errorf("not found")
}

func (s *StoreStorage) Del(id int) ([]model.Store, error) {
	s.Lock()
	defer s.Unlock()
	if len(s.Data) != 0 {
		res := make([]model.Store, len(s.Data)-1)
		if v, ok := s.primaryKeyIDx[id]; ok {
			for idx, elem := range s.Data {
				if elem == v {
					a, b := s.Data[:idx], s.Data[idx+1:]
					s.Data = append(a, b...)
					for i, e := range s.Data {
						res[i] = *e
					}
				}
			}

		} else {
			return []model.Store{}, errors.New("wrong id")
		}
		delete(s.primaryKeyIDx, id)
		return res, nil
	}
	return []model.Store{}, errors.New("wrong id")

}

func (s *StoreStorage) GetByStatus() map[string]int {
	s.Lock()
	defer s.Unlock()
	m := make(map[string]int, len(s.Data))
	for _, elem := range s.Data {
		m[elem.Status]++
	}
	return m
}
