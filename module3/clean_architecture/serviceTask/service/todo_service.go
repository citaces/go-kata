package service

import (
	model "gitlab.com/citaces/go-kata/module3/clean_architecture/serviceTask/model"
	repo "gitlab.com/citaces/go-kata/module3/clean_architecture/serviceTask/repo"
)

type Service interface {
	ListTodos() ([]model.Todo, error)
	CreateTodo(todo model.Todo) error
	CompleteTodo(id int) error
	RemoveTodo(todo model.Todo) error
	EditTodo(todo model.Todo) error
}

type TodoService struct {
	Repository repo.Repository
}

func NewTodoService(repo repo.Repository) Service {
	return &TodoService{repo}
}

func (s *TodoService) ListTodos() ([]model.Todo, error) {
	return s.Repository.GetTasks()
}

func (s *TodoService) CreateTodo(todo model.Todo) error {
	err := s.Repository.CreateTask(todo)
	if err != nil {
		return err
	}
	return nil
}

func (s *TodoService) CompleteTodo(id int) error {
	err := s.Repository.CompleteTask(id)
	if err != nil {
		return err
	}
	return nil
}

func (s *TodoService) RemoveTodo(todo model.Todo) error {
	err := s.Repository.DeleteTask(todo.ID)
	if err != nil {
		return err
	}
	return nil
}

func (s *TodoService) EditTodo(todo model.Todo) error {
	_, err := s.Repository.UpdateTask(todo)
	if err != nil {
		return err
	}
	return nil
}
