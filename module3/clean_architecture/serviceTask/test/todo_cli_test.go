package test

import (
	"fmt"
	"testing"

	"gitlab.com/citaces/go-kata/module3/clean_architecture/cliTask"
	"gitlab.com/citaces/go-kata/module3/clean_architecture/serviceTask/service"

	model "gitlab.com/citaces/go-kata/module3/clean_architecture/serviceTask/model"
	repo "gitlab.com/citaces/go-kata/module3/clean_architecture/serviceTask/repo"
)

func TestShowTasks(t *testing.T) {
	rep := &repo.TodoRepository{}
	rep.Tasks = []model.Todo{{ID: 1, Title: "title1", Description: "descr1", Status: "not completed"},
		{ID: 2, Title: "title2", Description: "descr2", Status: "not completed"},
		{ID: 3, Title: "title2", Description: "descr2", Status: "not completed"}}
	showTasks := cliTask.TodoCLI{Service: &service.TodoService{Repository: rep}}
	tasks, err := showTasks.Service.ListTodos()
	if err != nil {
		panic(err)
	}
	s := fmt.Sprintf("%v", tasks)
	expectingShowTasks(t, s, len(tasks))

}

func expectingShowTasks(t *testing.T, s string, length int) {
	if s != "[{1 title1 descr1 not completed} {2 title2 descr2 not completed} {3 title2 descr2 not completed}]" {
		t.Fail()
	}
	if length != 3 {
		t.Fail()
	}
}

func TestAddTask(t *testing.T) {
	rep := &repo.TodoRepository{}
	rep.Tasks = []model.Todo{{ID: 1, Title: "title1", Description: "descr1", Status: "not completed"},
		{ID: 2, Title: "title2", Description: "descr2", Status: "not completed"},
		{ID: 3, Title: "title2", Description: "descr2", Status: "not completed"}}

	addCLI := cliTask.TodoCLI{Service: &service.TodoService{Repository: rep}}
	err := addCLI.Service.CreateTodo(model.Todo{Title: "CREATED", Description: "CREATED"})
	if err != nil {
		panic(err)
	}
	tasks, err := addCLI.Service.ListTodos()
	if err != nil {
		panic(err)
	}
	s := fmt.Sprintf("%v", tasks)
	expectingAddTask(t, s, len(tasks))
}

func expectingAddTask(t *testing.T, s string, length int) {
	if s != "[{1 title1 descr1 not completed} {2 title2 descr2 not completed} {3 title2 descr2 not completed} {4 CREATED CREATED not completed}]" {
		t.Fail()
	}
	if length != 4 {
		t.Fail()
	}
}

func TestRemoveTask(t *testing.T) {
	rep := &repo.TodoRepository{}
	rep.Tasks = []model.Todo{{ID: 1, Title: "title1", Description: "descr1", Status: "not completed"},
		{ID: 2, Title: "title2", Description: "descr2", Status: "not completed"},
		{ID: 3, Title: "title2", Description: "descr2", Status: "not completed"}}

	addCLI := cliTask.TodoCLI{Service: &service.TodoService{Repository: rep}}
	err := addCLI.Service.RemoveTodo(model.Todo{ID: 2, Title: "title2", Description: "descr2", Status: "not completed"})
	if err != nil {
		panic(err)
	}
	tasks, err := addCLI.Service.ListTodos()
	if err != nil {
		panic(err)
	}
	s := fmt.Sprintf("%v", tasks)
	expectingRemoveTask(t, s, len(tasks))
}

func expectingRemoveTask(t *testing.T, s string, length int) {
	if s != "[{1 title1 descr1 not completed} {3 title2 descr2 not completed}]" {
		t.Fail()
	}
	if length != 2 {
		t.Fail()
	}
}

func TestEditTask(t *testing.T) {
	rep := &repo.TodoRepository{}
	rep.Tasks = []model.Todo{{ID: 1, Title: "title1", Description: "descr1", Status: "not completed"},
		{ID: 2, Title: "title2", Description: "descr2", Status: "not completed"},
		{ID: 3, Title: "title2", Description: "descr2", Status: "not completed"}}

	addCLI := cliTask.TodoCLI{Service: &service.TodoService{Repository: rep}}
	err := addCLI.Service.EditTodo(model.Todo{ID: 2, Title: "UPDATED", Description: "descr2", Status: "not completed"})
	if err != nil {
		panic(err)
	}
	tasks, err := addCLI.Service.ListTodos()
	if err != nil {
		panic(err)
	}
	s := fmt.Sprintf("%v", tasks[1].Title)
	expectingEditTask(t, s)
}

func expectingEditTask(t *testing.T, title string) {
	if title != "UPDATED" {
		t.Fail()
	}
}

func TestCompleteTask(t *testing.T) {
	rep := &repo.TodoRepository{}
	rep.Tasks = []model.Todo{{ID: 1, Title: "title1", Description: "descr1", Status: "not completed"},
		{ID: 2, Title: "title2", Description: "descr2", Status: "not completed"},
		{ID: 3, Title: "title2", Description: "descr2", Status: "not completed"}}

	addCLI := cliTask.TodoCLI{Service: &service.TodoService{Repository: rep}}
	err := addCLI.Service.CompleteTodo(2)
	if err != nil {
		panic(err)
	}
	tasks, err := addCLI.Service.ListTodos()
	if err != nil {
		panic(err)
	}
	s := fmt.Sprintf("%v", tasks[1].Status)
	expectingCompleteTask(t, s)
}

func expectingCompleteTask(t *testing.T, status string) {
	if status != "completed!" {
		t.Fail()
	}
}
