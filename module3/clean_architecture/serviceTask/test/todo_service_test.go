package test

import (
	"errors"
	"fmt"
	"testing"

	"gitlab.com/citaces/go-kata/module3/clean_architecture/serviceTask/service"

	model "gitlab.com/citaces/go-kata/module3/clean_architecture/serviceTask/model"
	repo "gitlab.com/citaces/go-kata/module3/clean_architecture/serviceTask/repo"
)

func TestListTodos(t *testing.T) {
	rep := &repo.TodoRepository{}
	rep.Tasks = []model.Todo{{ID: 1, Title: "title1", Description: "descr1", Status: "not completed"},
		{ID: 2, Title: "title2", Description: "descr2", Status: "not completed"},
		{ID: 3, Title: "title2", Description: "descr2", Status: "not completed"}}
	ser := service.NewTodoService(rep)
	findTask, err := ser.ListTodos()
	if err != nil {
		panic(err)
	}
	s := fmt.Sprintf("%v", findTask)
	expectingListTodos(t, s)
}

func TestUpdateTodos(t *testing.T) {
	rep := &repo.TodoRepository{}
	rep.Tasks = []model.Todo{{ID: 1, Title: "title1", Description: "descr1", Status: "not completed"},
		{ID: 2, Title: "title2", Description: "descr2", Status: "not completed"},
		{ID: 3, Title: "title2", Description: "descr2", Status: "not completed"}}
	ser := service.NewTodoService(rep)
	err := ser.EditTodo(model.Todo{ID: 2, Title: "EDITED", Description: "descr2", Status: "not completed"})
	if err != nil {
		panic(err)
	}
	s := fmt.Sprintf("%v", rep.Tasks[1].Title)
	expectingUpdateTodo(t, s)
}

func TestCreateTodo(t *testing.T) {
	rep := &repo.TodoRepository{}
	rep.Tasks = []model.Todo{{ID: 1, Title: "title1", Description: "descr1", Status: "not completed"}}
	ser := service.NewTodoService(rep)
	err := ser.CreateTodo(model.Todo{Title: "created", Description: "Task from service layout"})
	if err != nil {
		fmt.Println(errors.New("crush when created Todo test"))
	}
	s := fmt.Sprintf("%v", rep.Tasks)
	expectingCreateTodo(t, s)
}

func TestCompleteTodo(t *testing.T) {
	rep := &repo.TodoRepository{}
	rep.Tasks = []model.Todo{{ID: 1, Title: "title1", Description: "descr1", Status: "not completed"},
		{ID: 2, Title: "title2", Description: "descr2", Status: "not completed"},
		{ID: 3, Title: "title2", Description: "descr2", Status: "not completed"}}
	ser := service.NewTodoService(rep)
	err := ser.CompleteTodo(rep.Tasks[0].ID)
	if err != nil {
		panic(err)
	}
	expectingCompleteTodo(t, rep.Tasks[0].Status)
}

func TestRemoveTodo(t *testing.T) {
	rep := &repo.TodoRepository{}
	rep.Tasks = []model.Todo{{ID: 1, Title: "title1", Description: "descr1", Status: "not completed"},
		{ID: 2, Title: "title2", Description: "descr2", Status: "not completed"},
		{ID: 3, Title: "title2", Description: "descr2", Status: "not completed"}}
	ser := service.NewTodoService(rep)
	err := ser.RemoveTodo(rep.Tasks[1])
	if err != nil {
		panic(err)
	}
	expectingRemoveTodo(t, len(rep.Tasks))
}

func expectingListTodos(t *testing.T, s string) {
	if s != "[{1 title1 descr1 not completed} {2 title2 descr2 not completed} {3 title2 descr2 not completed}]" {
		t.Fail()
	}
}

func expectingCreateTodo(t *testing.T, s string) {
	if s != "[{1 title1 descr1 not completed} {2 created Task from service layout not completed}]" {
		t.Fail()
	}
}

func expectingCompleteTodo(t *testing.T, status string) {
	if status != "completed!" {
		t.Fail()
	}
}

func expectingRemoveTodo(t *testing.T, length int) {
	if length != 2 {
		t.Fail()
	}
}

func expectingUpdateTodo(t *testing.T, title string) {
	if title != "EDITED" {
		t.Fail()
	}
}
