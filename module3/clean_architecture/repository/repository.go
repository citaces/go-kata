// You can edit this code!
// Click here and start typing.
package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
)

type Repository interface {
	Save(record interface{}) error
	Find(id int) (interface{}, error)
	FindAll() ([]interface{}, error)
}

type UserRepository struct {
	File  io.ReadWriter
	Users []User
}

func NewUserRepository(file io.ReadWriter) *UserRepository {
	return &UserRepository{
		File: file,
	}
}

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// Загружать данные в конструкторе

func (r *UserRepository) Save(record interface{}) error {
	// logic to write a record as a JSON object to a file at r.FilePath

	user, ok := record.(User)
	if !ok {
		return errors.New("wrong record data")
	}

	user.ID = len(r.Users) + 1

	r.Users = append(r.Users, user)

	data, err := json.Marshal(&r.Users)
	if err != nil {
		return err
	}

	_, err = r.File.Write(data)

	if err != nil {
		return err
	}

	return nil
}

func (r *UserRepository) Find(id int) (interface{}, error) {
	// logic to read a JSON object from a file at r.FilePath and return the corresponding record

	users, err := r.FindAll()
	if err != nil {
		return nil, err
	}

	for _, u := range users {
		user, ok := u.(User)
		if !ok {
			return nil, errors.New("wrong data type")
		}
		if user.ID == id {
			return user, nil
		}
	}
	return nil, fmt.Errorf("user with this id = [%v] not found", id)
}

func (r *UserRepository) FindAll() ([]interface{}, error) {
	// logic to read all JSON objects from a file at r.FilePath and return a slice of records
	res := make([]interface{}, len(r.Users))
	if len(r.Users) == 0 {
		return nil, errors.New("empty")
	}
	for i, elem := range r.Users {
		res[i] = elem
	}
	return res, nil
}

func main() {
	data, err := os.OpenFile("module3/clean_architecture/repository/repo.json", os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		panic(err)
	}
	defer data.Close()

	rep := NewUserRepository(data)

	rep.Users = []User{{ID: 1, Name: "Hellmuth"}, {ID: 2, Name: "TonyG"}, {ID: 3, Name: "Dwan"}}

	additional := User{Name: "Moneymaker"}

	err = rep.Save(additional)

	if err != nil {
		panic(err)
	}

	findID, err := rep.Find(2)
	if err != nil {
		panic(err)
	}
	fmt.Println(findID)
	findAll, err := rep.FindAll()
	if err != nil {
		panic(err)
	}
	fmt.Println(findAll)
}
