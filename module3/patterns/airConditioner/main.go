package main

import (
	"fmt"
)

type AirConditioner interface {
	on()
	off()
	set(int)
}

type RealAirConditioner struct {
	temperature int
	status      bool
}

func (r *RealAirConditioner) on() {
	fmt.Println("Air conditioning is ON!")
	r.status = true
}
func (r *RealAirConditioner) off() {
	fmt.Println("Air conditioning is OFF!")
	r.status = false
}
func (r *RealAirConditioner) set(t int) {
	if r.status {
		r.temperature = t
		fmt.Printf("set temperature is %d degrees\n", r.temperature)
	} else {
		fmt.Println("ac is OFF! Unable to set temperature")
	}
}

type AirConditionerAdapter struct {
	airConditioner *RealAirConditioner
}

func (a *AirConditionerAdapter) on() {
	a.airConditioner.on()
}
func (a *AirConditionerAdapter) off() {
	a.airConditioner.off()
}
func (a *AirConditionerAdapter) set(t int) {
	a.airConditioner.set(t)
}

type AirConditionerProxy struct {
	adapter        *AirConditionerAdapter
	authenticated  bool
	accessLogCount int
}

func (a *AirConditionerProxy) on() {
	if a.authenticated {
		a.adapter.on()
	} else {
		fmt.Println("Failed to authenticate")
	}
	a.accessLogCount++
}
func (a *AirConditionerProxy) off() {
	if a.authenticated {
		a.adapter.off()
	} else {
		fmt.Println("Failed to authenticate")
	}
	a.accessLogCount++
}

func (a *AirConditionerProxy) set(t int) {
	if a.authenticated {
		a.adapter.set(t)
	} else {
		fmt.Println("Failed to authenticate")
	}
	a.accessLogCount++
}

func NewAirConditionerProxy(authenticated bool) *AirConditionerProxy {
	ac := &RealAirConditioner{}
	adapter := &AirConditionerAdapter{airConditioner: ac}
	return &AirConditionerProxy{
		adapter:       adapter,
		authenticated: authenticated,
	}
}

func main() {
	admin := NewAirConditionerProxy(true)
	admin.on()
	admin.off()
	admin.set(4)
	admin.on()
	admin.set(12)
	fmt.Printf("access log count by admin = %d\n", admin.accessLogCount)
	user := NewAirConditionerProxy(false)
	user.on()
	user.off()
	user.set(4)
	fmt.Printf("access log count by user = %d\n", user.accessLogCount)
}
