package main

import "fmt"

type PricingStrategy interface {
	Calculate(Order) float64
}

type Order struct {
	name   string
	price  float64
	amount float64
}

type RegularPricing struct {
}

func (r RegularPricing) Calculate(o Order) float64 {
	return o.price * o.amount
}

type SalePricing struct {
	percent float64
	name    string
}

func (s SalePricing) Calculate(o Order) float64 {
	discount := s.percent / 100
	return (1 - discount) * o.price * o.amount
}

func main() {
	o := &Order{name: "cheburek", price: 10, amount: 2}
	strategies := []PricingStrategy{RegularPricing{}, SalePricing{13, "постоянный покупатель"},
		SalePricing{7.2, "сезонная распородажа!"}}
	for _, strategy := range strategies {
		switch strategy := strategy.(type) {
		case RegularPricing:
			fmt.Printf("Наименование товара: %s, Стоимость: %.2f\n", o.name, strategy.Calculate(*o))
		case SalePricing:
			fmt.Printf("Наименование товара: %s, Стоимость с учетом скидки [%s]: %.2f\n",
				o.name, strategy.name, strategy.Calculate(*o))
		}
	}

}
