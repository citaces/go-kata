package main

import "fmt"

/*
11. Удалить листья с заданным значением
Дано root дерево бинарных данных и целочисленное значение target. Удалите все листовые узлы со значением target.
Обратите внимание, что после удаления листового узла со значением target его родительский узел становится листовым узлом со значением target и также должен быть удален (необходимо продолжать делать это, пока это будет невозможным).
Пример 1:
Input: root = [1,2,3,2,null,2,4], target = 2
Output: [1,null,3,null,4]
Объяснение: Листовые узлы зеленого цвета со значением (target= 2) удалены (изображение слева).
После удаления новые узлы становятся листовыми узлами со значением (target= 2) (изображение в центре).
Пример 2:
Input: root = [1,3,3,3,2], target = 3
Output: [1,3,null,null,2]
Пример 3:
Input: root = [1,2,null,2,null,2], target = 2
Output: [1]
Объяснение: Листовые узлы в зеленом со значением (target = 2) удаляются на каждом шаге.
Constraints:
Количество узлов в дереве находится в диапазоне [1, 3000].
1 <= Node.val, target <= 1000
https://leetcode.com/problems/delete-leaves-with-a-given-value/
*/

func removeLeafNodes(root *TreeNode, target int) *TreeNode {
	if root == nil {
		return nil
	}

	root.Left = removeLeafNodes(root.Left, target)
	root.Right = removeLeafNodes(root.Right, target)

	if root.Left == nil && root.Right == nil && root.Val == target {
		return nil
	}

	return root
}

func (n *TreeNode) ins(value int) *TreeNode {
	if n == nil {
		return &TreeNode{Val: value}
	}

	if value < n.Val {
		n.Left = n.Left.ins(value)
	} else {
		n.Right = n.Right.ins(value)
	}

	return n
}

func show(node *TreeNode) {
	if node == nil {
		return
	}
	fmt.Println(node.Val)
	show(node.Left)
	show(node.Right)
}

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func main() {
	root := &TreeNode{}
	for i := -2; i < 7; i++ {
		root.ins(i)
	}
	fmt.Println("Before:")
	show(root)

	root = removeLeafNodes(root, 6)

	fmt.Println("After:")
	show(root)
}
