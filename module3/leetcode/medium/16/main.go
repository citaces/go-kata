package main

import "fmt"

/*
16. Запросы подпрямоугольника
Реализуйте класс SubrectangleQueries, который принимает прямоугольник rows x cols в качестве матрицы целых
чисел в конструкторе и поддерживает два метода:
\1. updateSubrectangle(int row1, int col1, int row2, int col2, int newValue)
Обновляет все значения с newValue в подпрямоугольнике, верхняя левая координата которого (row1, col1),
а нижняя правая (row2, col2).
\2. getValue(int row, int col)
Возвращает текущее значение координаты (row, col) из прямоугольника.
Пример 1:
Input
["SubrectangleQueries","getValue","updateSubrectangle","getValue","getValue","updateSubrectangle","getValue","getValue"]
[[[[1,2,1],[4,3,4],[3,2,1],[1,1,1]]],[0,2],[0,0,3,2,5],[0,2],[3,1],[3,0,3,2,10],[3,1],[0,2]]
Output
[null,1,null,5,5,null,10,5]
Объяснение:
SubrectangleQueries subrectangleQueries = new SubrectangleQueries([[1,2,1],[4,3,4],[3,2,1],[1,1,1]]);
// Начальный прямоугольник (4х3) выглядит так::
// 1 2 1
// 4 3 4
// 3 2 1
// 1 1 1
subrectangleQueries.getValue(0, 2); // return 1
subrectangleQueries.updateSubrectangle(0, 0, 3, 2, 5);
// После этого обновления прямоугольник выглядит следующим образом:
// 5 5 5
// 5 5 5
// 5 5 5
// 5 5 5
subrectangleQueries.getValue(0, 2); // return 5
subrectangleQueries.getValue(3, 1); // return 5
subrectangleQueries.updateSubrectangle(3, 0, 3, 2, 10);
// После этого обновления прямоугольник выглядит следующим образом:
// 5 5 5
// 5 5 5
// 5 5 5
// 10 10 10
subrectangleQueries.getValue(3, 1); // return 10
subrectangleQueries.getValue(0, 2); // return 5
Пример 2:
Input
["SubrectangleQueries","getValue","updateSubrectangle","getValue","getValue","updateSubrectangle","getValue"]
[[[[1,1,1],[2,2,2],[3,3,3]]],[0,0],[0,0,2,2,100],[0,0],[2,2],[1,1,2,2,20],[2,2]]
Output
[null,1,null,100,100,null,20]
Объяснение:
SubrectangleQueries subrectangleQueries = new SubrectangleQueries([[1,1,1],[2,2,2],[3,3,3]]);
subrectangleQueries.getValue(0, 0); // return 1
subrectangleQueries.updateSubrectangle(0, 0, 2, 2, 100);
subrectangleQueries.getValue(0, 0); // return 100
subrectangleQueries.getValue(2, 2); // return 100
subrectangleQueries.updateSubrectangle(1, 1, 2, 2, 20);
subrectangleQueries.getValue(2, 2); // return 20
Constraints:
Будет не более 500 операций, учитывая оба метода: updateSubrectangle и getValue.
1 <= rows, cols <= 100
rows == rectangle.length
cols == rectangle[i].length
0 <= row1 <= row2 < rows
0 <= col1 <= col2 < cols
1 <= newValue, rectangle[i][j] <= 10^9
0 <= row < rows
0 <= col < cols
https://leetcode.com/problems/subrectangle-queries/
*/

type SubrectangleQueries struct {
	ms [][]int
}

func Constructor(rectangle [][]int) SubrectangleQueries {
	return SubrectangleQueries{ms: rectangle}
}
func (this *SubrectangleQueries) UpdateSubrectangle(row1 int, col1 int, row2 int, col2 int, newValue int) {
	for i := row1; i <= row2; i++ {
		for j := col1; j <= col2; j++ {
			this.ms[i][j] = newValue
		}
	}
}
func (this *SubrectangleQueries) GetValue(row int, col int) int {
	return this.ms[row][col]
}

func main() {
	x := Constructor([][]int{{10, 9, 1, 7, 4}, {6, 2, 4, 10, 7}, {10, 4, 8, 2, 7}})
	x.UpdateSubrectangle(1, 1, 1, 1, 10)
	fmt.Println(x)

	x = Constructor([][]int{{1, 2, 3}, {4, 228, 6}})
	x.UpdateSubrectangle(0, 0, 1, 0, 5)
	fmt.Println(x)
	fmt.Println(x)

	//x.UpdateSubrectangle(3, 0, 3, 2, 10)
	//fmt.Println(x)
	//x = Constructor([][]int{{1, 1, 1}, {2, 2, 2}, {3, 3, 3}})
	//x.UpdateSubrectangle(0, 1, 2, 1, 100)
	//fmt.Println(x)

}
