package main

/*
3. Объединить узлы
Вам дан заголовок связанного списка, который содержит ряд целых чисел, разделенных нулями.
Начало и конец связного списка будет иметь Node.val == 0.
Для каждых двух последовательных нулей объединяйте все узлы, лежащие между ними в один узел,
чье значение является суммой всех объеденных узлов. Измененный список не должен содержать
никаких нулей.
Верните заголовок измененного связного списка.
Пример 1:
Input: head = [0,3,1,0,4,5,2,0]
Output: [4,11]
Объяснение: Вышеуказанная фигура представляет собой связанный список. Измененный список содержит:
- Сумму узлов, помеченных зеленым: 3 + 1 = 4.
- Сумму узлов, помеченных красным: 4 + 5 + 2 = 11.
Пример 2:
Input: head = [0,1,0,3,0,2,2,0]
Output: [1,3,4]
Объяснение:
Вышеуказанная фигура представляет собой данный связанный список. Измененный список содержит:
- Сумму узлов, отмеченных зеленым цветом: 1 = 1.
- Сумму узлов, отмеченных красным цветом: 3 = 3.
- Сумму узлов, отмеченных желтым цветом: 2 + 2 = 4.
Constraints:
Количество узлов в списке находится в диапазоне [3, 2 * 105].
0 <= Node.val <= 1000
Нет двух последовательных узлов с Node.val == 0.
Начало и конец связного списка имеют Node.val == 0.
https://leetcode.com/problems/merge-nodes-in-between-zeros/
*/

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */

type ListNode struct {
	Val  int
	Next *ListNode
}

func mergeNodes(head *ListNode) *ListNode {
	nN := make([]int, 0)
	curr := head.Next
	count := 0
	for curr != nil {
		if curr.Val == 0 {
			nN = append(nN, count)
			count = 0
		}
		count += curr.Val
		curr = curr.Next
	}

	headNew := &ListNode{}
	currentNew := headNew

	for i := 0; i < len(nN); i++ {
		newNode := &ListNode{Val: nN[i]}
		currentNew.Next = newNode
		currentNew = newNode
	}
	return headNew.Next
}

func main() {
	//0,1,0,3,0,2,2,0
	node := &ListNode{}
	node.Val = 0
	node.Next = &ListNode{}
	node.Next.Val = 1
	node.Next.Next = &ListNode{}
	node.Next.Next.Val = 0
	node.Next.Next.Next = &ListNode{}
	node.Next.Next.Next.Val = 3
	node.Next.Next.Next.Next = &ListNode{}
	node.Next.Next.Next.Next.Val = 0
	node.Next.Next.Next.Next.Next = &ListNode{}
	node.Next.Next.Next.Next.Next.Val = 2
	node.Next.Next.Next.Next.Next.Next = &ListNode{}
	node.Next.Next.Next.Next.Next.Next.Val = 2
	node.Next.Next.Next.Next.Next.Next.Next = &ListNode{}
	node.Next.Next.Next.Next.Next.Next.Next.Val = 0
	mergeNodes(node)
}
