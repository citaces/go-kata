package main

import "fmt"

/*
7. Балансировка бинарного поискового дерева
Дано корневое значение бинарного поискового дерева. Верните сбалансированное бинарное поисковое дерево с теми же значениями узлов. Если есть более одного ответа, верните любой из них.
Бинарное поисковое дерево считается сбалансированным, если глубина двух поддеревьев каждого узла никогда не отличается более чем на 1.
Пример 1:
Input: root = [1,null,2,null,3,null,4,null,null]
Output: [2,1,3,null,null,null,4]
Explanation: Это не единственный правильный ответ, [3,1,4,null,2] также правильный.
Пример 2:
Input: root = [2,1,3]
Output: [2,1,3]
Constraints:
Количество узлов в дереве находится в диапазоне [1, 104].
1 <= Node.val <= 105
https://leetcode.com/problems/balance-a-binary-search-tree/
*/

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func (n *TreeNode) Insert(value int) *TreeNode {
	if n == nil {
		return &TreeNode{Val: value}
	}

	if value < n.Val {
		n.Left = n.Left.Insert(value)
	} else {
		n.Right = n.Right.Insert(value)
	}

	return n
}

func balanceBST(root *TreeNode) *TreeNode {
	values := Values(root)

	return sortedArrayToBST(values, 0, len(values)-1)
}

func Values(t *TreeNode) []int {
	if t == nil {
		return []int{}
	}
	left := Values(t.Left)
	right := Values(t.Right)

	return append(append(left, t.Val), right...)
}

func sortedArrayToBST(values []int, start int, end int) *TreeNode {
	if start > end {
		return nil
	}

	mid := (start + end) / 2
	node := &TreeNode{Val: values[mid]}
	node.Left = sortedArrayToBST(values, start, mid-1)
	node.Right = sortedArrayToBST(values, mid+1, end)
	return node
}

func (t *TreeNode) show() {
	if t == nil {
		return
	}
	t.Left.show()
	fmt.Println(t.Val)
	t.Right.show()
}

func main() {
	root := &TreeNode{Val: 4}
	root.Insert(5).Insert(10).Insert(2).Insert(-10)
	root.show()
	x := balanceBST(root)
	fmt.Println(x)
	x.show()
}
