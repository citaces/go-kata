package main

import (
	"fmt"
	"math"
)

/*
1. Сумма самых глубоких листьев.
Дан root дерево бинарных данных, верните сумму значений его самых глубоких листьев.
Пример 1:
Input: root = [1,2,3,4,5,null,6,7,null,null,null,null,8]
Output: 15
Пример 2:
Input: root = [,,8,,,,9,null,,,null,null,null,]
Output: 19
Constraints:
Количество узлов в дереве находится в диапазоне [1, 104].
1 <= Node.val <= 100
https://leetcode.com/problems/deepest-leaves-sum/

 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
*/

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func maxDepth(node *TreeNode) float64 {
	if node == nil {
		return 0
	}
	return math.Max(maxDepth(node.Left), maxDepth(node.Right)) + 1
}

func sumDeepestLeaves(node *TreeNode, depth float64, maxDepth float64) int {
	if node == nil {
		return 0
	}
	if depth == maxDepth {
		return node.Val
	}
	return sumDeepestLeaves(node.Left, depth+1, maxDepth) + sumDeepestLeaves(node.Right, depth+1, maxDepth)
}

func deepestLeavesSum(root *TreeNode) int {
	deepestDepth := maxDepth(root)
	return sumDeepestLeaves(root, 1, deepestDepth)
}

func main() {
	root := &TreeNode{Val: 1}
	root.Left = &TreeNode{Val: 2}
	root.Right = &TreeNode{Val: 3}
	root.Left.Left = &TreeNode{Val: 4}
	root.Left.Left = &TreeNode{Val: 5}
	root.Left.Right = &TreeNode{}
	root.Right.Right = &TreeNode{Val: 6}
	root.Left.Right.Left = &TreeNode{Val: 7}
	root.Right.Right.Right = &TreeNode{Val: 9}
	fmt.Println(deepestLeavesSum(root))
}
