package main

import "fmt"

/*
4. Бинарное дерево в большую сумму дерева.
Дано root звено бинарного поискового дерева (BST), преобразуйте его в большее дерево так, чтобы
каждый ключ исходного BST был изменен на исходный ключ плюс сумма всех ключей, больших исходного ключа в BST.
В качестве напоминания, бинарное поисковое дерево - это дерево, которое удовлетворяет следующим ограничениям:
Левое поддерево узла содержит только узлы с ключами меньше ключа узла.
Правое поддерево узла содержит только узлы с ключами больше ключа узла.
Оба поддерева должны быть также бинарными поисковыми деревьями.
Пример 1:
Input: root = [4,1,6,0,2,5,7,null,null,null,3,null,null,null,8]
Output: [,,,,,,,null,null,null,,null,null,null,]
Пример 2:
Input: root = [0,null,1]
Output: [1,null,1]
Constraints:
Диапазон значений узлов находится в диапазоне [1, 100]
0 <= Node.val <= 100
Все значения в дереве уникальны.
https://leetcode.com/problems/binary-search-tree-to-greater-sum-tree/
*/

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func Format(root *TreeNode, Slice *[]int) {
	if root == nil {
		return
	}

	Format(root.Left, Slice)
	*Slice = append(*Slice, root.Val)
	Format(root.Right, Slice)
}

func bigSum(root *TreeNode, m map[int]int) {
	if root == nil {
		return
	}

	bigSum(root.Left, m)

	if val, ok := m[root.Val]; ok {
		root.Val = val
	}

	bigSum(root.Right, m)
}

func bstToGst(root *TreeNode) *TreeNode {
	sl := make([]int, 0)
	Format(root, &sl)
	m := make(map[int]int)
	for _, elem := range sl {
		count := elem
		for _, val := range sl {
			if elem < val {
				count += val
			}
		}
		m[elem] = count
	}

	bigSum(root, m)

	return root

}

func Add(root *TreeNode, n int) {
	if root == nil {
		return
	}
	if root.Val < n {
		if root.Right == nil {
			root.Right = &TreeNode{Val: n}
		}
		Add(root.Right, n)
	}
	if root.Val > n {
		if root.Left == nil {
			root.Left = &TreeNode{Val: n}
		}
		Add(root.Left, n)
	}
}

func main() {
	root := &TreeNode{}
	Add(root, 4)
	Add(root, 1)
	Add(root, 6)
	Add(root, 0)
	Add(root, 2)
	Add(root, 5)
	Add(root, 7)
	Add(root, 3)
	Add(root, 8)
	fmt.Println(bstToGst(root).Right.Val)
}
