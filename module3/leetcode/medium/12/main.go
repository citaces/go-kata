package main

import "fmt"

/*
12. Объединить между связанными списками
Вам даны два связанных списка: list1 и list2 по размерам n и m соответственно. Удалите узлы списка 1 с ath узла до bth узла и поместите list2 на их место. Синие ребра и узлы на рисунке ниже показывают результат:
Собрать список результатов и вернуть его заголовок.
Пример 1:
Input: list1 = [0,1,2,3,4,5], a = 3, b = 4, list2 = [1000000,1000001,1000002]
Output: [0,1,2,1000000,1000001,1000002,5]
Объяснение: Мы удаляем узлы 3 и 4 и помещаем весь list2 на их место. Синие края и узлы на рисунке выше показывают результат.
Пример 2:
Input: list1 = [0,1,2,3,4,5,6], a = 2, b = 5, list2 = [1000000,1000001,1000002,1000003,1000004]
Output: [0,1,1000000,1000001,1000002,1000003,1000004,6]
Объяснение: Синие рёбра и узлы на рисунке выше обозначают результат.
Constraints:
3 <= list1.length <= 104
1 <= a <= b < list1.length - 1
1 <= list2.length <= 104
https://leetcode.com/problems/merge-in-between-linked-lists/
*/

type ListNode struct {
	Val  int
	Next *ListNode
}

func mergeInBetween(list1 *ListNode, a int, b int, list2 *ListNode) *ListNode {

	merged := &ListNode{}
	curr := merged

	current := list1
	count := 0

	for current != nil {
		if count == a {
			break
		}

		node := &ListNode{Val: current.Val}
		curr.Next = node
		curr = curr.Next

		current = current.Next
		count++

	}

	current = list2
	for current != nil {

		node := &ListNode{Val: current.Val}
		curr.Next = node
		curr = curr.Next

		current = current.Next
	}

	current = list1
	count = 0

	for current != nil {
		if count > b {

			node := &ListNode{Val: current.Val}
			curr.Next = node
			curr = curr.Next

		}

		current = current.Next
		count++
	}

	merged = merged.Next

	return merged
}

func main() {
	sl1 := []int{0, 1, 2, 3, 4, 5, 6}
	sl2 := []int{1000000, 1000001, 1000002, 1000003, 1000004}
	a, b := 2, 5

	list1 := &ListNode{}
	list1.Val = sl1[0]
	current := list1
	list2 := &ListNode{}
	list2.Val = sl2[0]
	current2 := list2
	for idx, elem := range sl1 {
		if idx == 0 {
			continue
		}

		node := &ListNode{Val: elem}
		current.Next = node
		current = current.Next

	}

	for idx, elem := range sl2 {

		if idx == 0 {
			continue
		}
		node := &ListNode{Val: elem}
		current2.Next = node
		current2 = current2.Next
	}

	x := mergeInBetween(list1, a, b, list2)

	head := x

	for head != nil {
		fmt.Printf("%v\n", head.Val)

		head = head.Next
	}
}
