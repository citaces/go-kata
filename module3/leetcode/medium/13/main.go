package main

import "fmt"

/*
13. Найти максимальную сумму песочных часов
Вам дана целочисленная матрица m x n.
Мы определяем песочные часы как часть матрицы со следующей формой:
Вернуть максимальную сумму элементов песочных часов.
Обратите внимание, что песочные часы не могут быть повернуты и должны быть полностью содержаться в матрице.
 Пример 1:
Input: grid = [[6,2,1,3],[4,2,1,5],[9,2,8,7],[4,1,2,9]]
Output: 30
Объяснение: Ячейки, показанные выше, представляют часы с максимальной суммой: 6 + 2 + 1 + 2 + 9 + 2 + 8 = 30.
Пример 2:
Input: grid = [[1,2,3],[4,5,6],[7,8,9]]
Output: 35
Объяснение: В матрице есть только одна фигура песочных часов, сумма которой составляет: 1 + 2 + 3 + 5 + 7 + 8 + 9 = 35.
Constraints:
m == grid.length
n == grid[i].length
3 <= m, n <= 150
0 <= grid[i][j] <= 106
https://leetcode.com/problems/maximum-sum-of-an-hourglass/


*/

func maxSum(grid [][]int) int {
	count := 0
	max := 0
	for i := 0; i < len(grid)-2; i++ {
		for z := 0; z < len(grid[i])-2; z++ {
			count = grid[i][z] + grid[i][z+1] + grid[i][z+2] + grid[i+1][z+1] + grid[i+2][z] + grid[i+2][z+1] + grid[i+2][z+2]
			if max <= count {
				max = count
			}
		}

	}
	return max
}

func main() {
	//fmt.Println(maxSum([][]int{{1, 2, 3, 4, 5, 6}, {8, 2, 3, 4, 5, 7}, {1, 7, 3, 4, 5, 8}}))
	//fmt.Println(maxSum([][]int{{1, 2, 3}, {1, 2, 3}, {1, 2, 3}}))
	fmt.Println(maxSum([][]int{{6, 2, 1, 3}, {4, 2, 1, 5}, {9, 2, 8, 7}, {4, 1, 2, 9}}))

	//fmt.Println(maxSum([][]int{{520626, 685427, 788912, 800638, 717251, 683428}, {23602, 608915, 697585, 957500, 154778, 209236}, {287585, 588801, 818234, 73530, 939116, 252369}, {520626, 685427, 788912, 800638, 717251, 683428}, {23602, 608915, 697585, 957500, 154778, 209236}, {287585, 588801, 818234, 73530, 939116, 252369}}))
}
