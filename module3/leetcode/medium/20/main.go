package main

import "fmt"

/*
20. Запросы на перестановку с ключом
Дан массив запросов из положительных целых чисел между 1 и m, вам необходимо обработать все запросы [i]
(от i = 0 до i = queries.length-1) в соответствии со следующими правилами:
Вначале у вас есть перестановка P = [1,2,3,...,m].
Для текущего i найдите позицию запросов [i] в перестановке P (индексирование с 0) и переместите его в начало
перестановки P. Обратите внимание, что позиция запросов [i] в P является результатом для запросов [i].
Верните массив, содержащий результат для данных запросов.
Пример 1:
Input: queries = [3,1,2,1], m = 5
Output: [2,1,2,1]
Объяснение: Запросы обрабатываются следующим образом:
Для i = 0: запросы [i] = 3, P = [1,2,3,4,5], положение 3 в P равно 2, затем мы перемещаем 3 в начало P, получая P = [3,1,2,4,5].
Для i = 1: запросы [i] = 1, P = [3,1,2,4,5], положение 1 в P равно 1, затем мы перемещаем 1 в начало P, получая P = [1,3,2,4,5].
Для i = 2: запросы [i] = 2, P = [1,3,2,4,5], положение 2 в P равно 2, затем мы перемещаем 2 в начало P, получая P = [2,1,3,4,5].
Для i = 3: запросы [i] = 1, P = [2,1,3,4,5], положение 1 в P равно 1, затем мы перемещаем 1 в начало P, получая P = [1,2,3,4,5].
Поэтому массив, содержащий результат, равен [2,1,2,1].
Пример 2:
Input: queries = [4,1,2,2], m = 4
Output: [3,1,2,0]
Пример 3:
Input: queries = [7,5,5,8,3], m = 8
Output: [6,5,0,7,5]
Constraints:
1 <= m <= 10^3
1 <= queries.length <= m
1 <= queries[i] <= m
https://leetcode.com/problems/queries-on-a-permutation-with-key/
*/

func processQueries(queries []int, m int) []int {
	P := make([]int, m)
	cache := make([]int, 0)
	mp := make(map[int]int, m)
	res := make([]int, 0)
	for i := 0; i < len(P); i++ {
		P[i] = i + 1
	}
	for i, elem := range queries {
		mp[i] = elem
	}

	for i := 0; i < len(queries); i++ {
		if val, ok := mp[i]; ok {
			for index, elem := range P {
				if elem == val {
					res = append(res, index)
					cache = append(cache, elem)
					cache := append(cache, P[:index]...)
					cache = append(cache, P[index+1:]...)
					P = cache
				}
			}
			cache = nil
		}
	}
	return res
}

func main() {
	fmt.Println(processQueries([]int{3, 1, 2, 1}, 5))
	fmt.Println(processQueries([]int{4, 1, 2, 2}, 4))
	fmt.Println(processQueries([]int{7, 5, 5, 8, 3}, 8))
}
