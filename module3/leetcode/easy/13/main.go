package main

import (
	"fmt"
	"strings"
)

/*
13. Драгоценности и камни
Вам даны строки jewels, представляющие типы камней, которые являются драгоценностями, и камни,
представляющие stones, которые у вас есть. Каждый символ в stones является типом камня, который
у вас есть. Вы хотите знать, сколько из камней, которые у вас есть, также являются драгоценностями.
Буквы чувствительны к регистру, поэтому «a» считается разным типом камня от «A».
Пример 1:
Input: jewels = "aA", stones = "aAAbbbb"
Output: 3
Пример 2:
Input: jewels = "z", stones = "ZZ"
Output: 0
Ограничения:
1 <= jewels.length, stones.length <= 50
jewels и stones состоят только из английских букв.
Все символы jewels уникальны.

*/

func numJewelsInStones(jewels string, stones string) int {
	count := 0
	for _, elem := range stones {
		if strings.Contains(jewels, string(elem)) {
			count++
		}
	}
	return count
}

func main() {
	fmt.Println(numJewelsInStones("aA", "aAAbbbb"))
}
