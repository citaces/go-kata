package main

import "fmt"

/*
15. Дизайн парковочной системы
Спроектировать парковочную систему для парковки. На парковке есть три вида парковочных мест: большие,
средние и маленькие, с фиксированным количеством мест для каждого размера.
Реализуйте класс ParkingSystem:
ParkingSystem(int big, int medium, int small) Инициализирует объект класса ParkingSystem. Количество мест для
каждого парковочного места указано в конструкторе.
bool addCar(int carType) Проверяет, есть ли парковочное место типа carType для автомобиля, который хочет попасть на
стоянку. carType может быть трех видов: большой, средний или маленький, которые представлены цифрами 1, 2 и 3
соответственно. Автомобиль может парковаться только на парковочном месте своего типа автомобиля carType.
Если места нет, верните false, в противном случае припаркуйте машину на месте такого размера и верните true.
Пример 1:
Input
["ParkingSystem", "addCar", "addCar", "addCar", "addCar"]
[[1, 1, 0], [1], [2], [3], [1]]
Output
[null, true, true, false, false]
Объяснение
ParkingSystemparkingSystem = новая ParkingSystem(1, 1, 0);
parkingSystem.addCar(1); // возвращаем true, потому что есть 1 свободный слот для большой машины
parkingSystem.addCar(2); // вернем true, потому что есть 1 свободный слот для средней машины
parkingSystem.addCar(3); // вернем false, потому что нет свободного слота для маленькой машины
parkingSystem.addCar(1); // вернем false, потому что нет свободного места для большой машины. Оно уже занято.
Ограничения:
0 <= большой, средний, маленький <= 1000
carType — 1, 2 или 3.
В addCar будет сделано не более 1000 запросов.
https://leetcode.com/problems/design-parking-system/
*/

type ParkingSystem struct {
	big, medium, small int
}

func Constructor(big int, medium int, small int) ParkingSystem {
	return ParkingSystem{small, medium, big}
}

func (p *ParkingSystem) AddCar(carType int) bool {
	switch carType {
	case 1:
		p.small--
		if p.small < 0 {
			return false
		}
	case 2:
		p.medium--
		if p.medium < 0 {
			return false
		}
	case 3:
		p.big--
		if p.big < 0 {
			return false
		}
	}
	return true
}

func main() {
	parkingSystem := Constructor(1, 1, 0)
	fmt.Println(parkingSystem.AddCar(1))
	fmt.Println(parkingSystem.AddCar(2))
	fmt.Println(parkingSystem.AddCar(3))
	fmt.Println(parkingSystem.AddCar(1))
}
