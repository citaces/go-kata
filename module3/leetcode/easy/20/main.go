package main

import "fmt"

/*
20. Дети с наибольшим количеством конфет
У вас есть n детей с конфетами. Вам дан целочисленный массив candies, где candies [i] представляет количество конфет,
которые имеет n-й ребенок, и целое число extraCandies, указывающее количество дополнительных конфет, которые у вас есть.
Верните логический массив result длиной n, где result [i] равен true, если, после того, как вы дадите n-му ребенку все
extraCandies, они будут иметь наибольшее количество конфет среди всех детей, или false в противном случае.
Обратите внимание, что у нескольких детей может быть наибольшее количество конфет.
Пример 1:
Input: candies = [2,3,5,1,3], extraCandies = 3
Output: [true,true,true,false,true]
Объяснение: Если вы дадите все extraCandies:
- Ребенок 1 будет иметь 2 + 3 = 5 конфет, что является наибольшим среди детей.
- Ребенок 2 будет иметь 3 + 3 = 6 конфет, что является наибольшим среди детей.
- Ребенок 3 будет иметь 5 + 3 = 8 конфет, что является наибольшим среди детей.
- Ребенок 4 будет иметь 1 + 3 = 4 конфеты, что не является наибольшим среди детей.
- Ребенок 5 будет иметь 3 + 3 = 6 конфет, что является наибольшим среди детей.
Пример 2:
Input: candies = [4,2,1,1,2], extraCandies = 1
Output: [true,false,false,false,false]
Объяснение: Есть только 1 дополнительная конфета.
Ребенок 1 всегда будет иметь наибольшее количество конфет, даже если другой ребенок получит дополнительную конфету.
Пример 3:
Input: candies = [12,1,12], extraCandies = 10
Output: [true,false,true]
Ограничения:
n == candies.length
2 <= n <= 100
1 <= candies[i] <= 100
1 <= extraCandies <= 50
https://leetcode.com/problems/kids-with-the-greatest-number-of-candies/
*/

func kidsWithCandies(candies []int, extraCandies int) []bool {
	bl := make([]bool, 0, len(candies))
	max := 0
	for _, candy := range candies {
		if max <= candy {
			max = candy
		}
	}
	for _, candy := range candies {
		if candy+extraCandies >= max {
			bl = append(bl, true)
		} else {
			bl = append(bl, false)
		}
	}
	return bl
}

func main() {
	fmt.Println(kidsWithCandies([]int{2, 3, 5, 1, 3}, 3))
	fmt.Println(kidsWithCandies([]int{4, 2, 1, 1, 2}, 1))
}
