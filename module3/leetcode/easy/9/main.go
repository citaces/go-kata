package main

import (
	"fmt"
)

/*
9. Финальное значение переменной после выполнения операций
Есть язык программирования с только четырьмя операциями и одной переменной X:
++X и X ++ увеличивает значение переменной X на 1.
--X и X -- уменьшает значение переменной X на 1.
Изначально значение X равно 0.
Дан массив строк operations, содержащий список операций, возвращает финальное значение X после выполнения всех операций.
Пример 1:
Input: operations = ["--X","X ++","X ++"]
Output: 1
Объяснение: Операции выполняются следующим образом:
Изначально X = 0.
--X: X уменьшается на 1, X = 0 - 1 = -1.
X ++: X увеличивается на 1, X = -1 + 1 = 0.
X ++: X увеличивается на 1, X = 0 + 1 = 1.
Пример 2:
Input: operations = ["++X","++X","X ++"]
Output: 3
Объяснение: Операции выполняются следующим образом:
Изначально X = 0.
++X: X увеличивается на 1, X = 0 + 1 = 1.
++X: X увеличивается на 1, X = 1 + 1 = 2.
X ++: X увеличивается на 1, X = 2 + 1 = 3.
Пример 3:
Input: operations = ["X++","++X","--X","X--"]
Output: 0
Объяснение: Операции выполняются следующим образом:
Изначально, X = 0.
X++: X увеличивается на 1, X = 0 + 1 = 1.
++X: X увеличивается на 1, X = 1 + 1 = 2.
--X: X уменьшается на 1, X = 2 - 1 = 1.
X--: X уменьшается на 1, X = 1 - 1 = 0.
Ограничения:
1 <= operations.length <= 100

operations[i] будет либо "++X", "X++", "--X", либо "X--".

https://leetcode.com/problems/final-value-of-variable-after-performing-operations/
*/
func finalValueAfterOperations(operations []string) int {
	var count int
	for _, elem := range operations {
		switch elem {
		case "++X":
			count++
		case "X++":
			count++
		case "--X":
			count--
		case "X--":
			count--
		}
	}
	return count
}

func main() {
	fmt.Println(finalValueAfterOperations([]string{"++X", "++X", "X ++", "--X", "X --"}))
	fmt.Println(finalValueAfterOperations([]string{"--X", "X++", "X++"}))

}
