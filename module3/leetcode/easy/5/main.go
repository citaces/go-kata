package main

import "fmt"

/*
5. Количество матчей в турнире
Дано целое число n - количество команд в турнире с необычными правилами:
Если текущее количество команд четное, то каждая команда играет в паре с другой
командой. Всего сыграно n / 2 матчей, и n / 2 команды проходят на следующий раунд.
Если текущее количество команд нечетное, то одна команда проходит дальше в турнире
случайным образом, а остальные команды играют в паре. Всего сыграно (n-1) / 2 матчей,
и (n-1) / 2 + 1 команда проходит на следующий раунд.
Вернуть количество матчей, сыгранных в турнире, пока не будет определен победитель.
Пример 1:
Input: n = 7
Output: 6
Объяснение: Детали турнира:
1-й раунд: команды = 7, матчей = 3, и 4 команды проходят дальше.
2-й раунд: команды = 4, матчей = 2, и 2 команды проходят дальше.
3-й раунд: команды = 2, матчей = 1, и 1 команда объявляется победителем. Всего сыграно матчей = 3 + 2 + 1 = 6.
Пример 2:
Input: n = 14
Output: 13
Объяснение: Детали турнира:
1-й раунд: команды = 14, матчей = 7, и 7 команд проходят дальше.
2-й раунд: команды = 7, матчей = 3, и 4 команды проходят дальше.
3-й раунд: команды = 4, матчей = 2, и 2 команды проходят дальше.
4-й раунд: команды = 2, матчей = 1, и 1 команда объявляется победителем. Всего сыграно матчей = 7 + 3 + 2 + 1 = 13.
Ограничения:
1 <= n <= 200
*/
func numberOfMatches(n int) int {
	var team, count int
	for n != 1 {
		team = n / 2
		count += team
		switch {
		case n%2 == 0:
			n /= 2
		default:
			n = (n / 2) + 1
		}
	}
	return count
}

func main() {
	fmt.Println(numberOfMatches(1))
	fmt.Println(numberOfMatches(2))
	fmt.Println(numberOfMatches(3))
	fmt.Println(numberOfMatches(4))
	fmt.Println(numberOfMatches(5))
	fmt.Println(numberOfMatches(7))
	fmt.Println(numberOfMatches(14))
}
