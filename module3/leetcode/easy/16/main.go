package main

import "fmt"

/*
16. Наименьшее четное кратное
Дано положительное целое число n. Верните наименьшее положительное целое число, которое является кратным как 2, так и n.
Пример 1:
Input: n = 5
Output: 10
Объяснение: Наименьшим кратным числом для 5 и 2 является 10.
Пример 2:
Input: n = 6
Output: 6
Объяснение: Наименьшим кратным числом для 6 и 2 является 6. Обратите внимание, что число является кратным само себе.
Ограничения:
1 <= n <= 150
https://leetcode.com/problems/smallest-even-multiple/
*/

func smallestEvenMultiple(n int) int {
	if n%2 == 0 {
		return n
	} else {
		return n * 2
	}
}

func main() {
	fmt.Println(smallestEvenMultiple(10))
	fmt.Println(smallestEvenMultiple(6))
}
