package main

import "fmt"

/*
1. N-th Tribonacci Number

Последовательность Трибоначчи Tn определяется следующим образом:
T0 = 0, T1 = 1, T2 = 1 и Tn + 3 = Tn + Tn + 1 + Tn + 2 для n> = 0.
Дано n, верните значение Tn.
Пример 1:
Input: n = 4
Output: 4
Объяснение:
T_3 = 0 + 1 + 1 = 2
T_4 = 1 + 1 + 2 = 4
Пример 2:
Input: n = 25
Output: 1389537
Ограничения:
0 <= n <= 37
Ответ гарантированно поместится в 32-битное целое число, т.е. ответ <= 2 ^ 31 - 1.
*/

func tribonacci(n int) int {
	var t0, t1, t2, tr int
	t0 = 0
	t1 = 1
	t2 = 1
	if n == 1 || n == 2 {
		return 1
	}
	for i := 3; i <= n; i++ {
		tr = t0 + t1 + t2
		t0, t1, t2 = t1, t2, tr
	}
	return tr
}

func main() {
	fmt.Println(tribonacci(25))
}
