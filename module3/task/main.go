package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"os"
	"strings"
	"time"

	"github.com/brianvoe/gofakeit"
)

type DoubleLinkedList struct {
	head *Node // начальный элемент в списке
	tail *Node // последний элемент в списке
	curr *Node // текущий элемент меняется при использовании методов next, prev
	len  int   // количество элементов в списке
}

type LinkedLister interface {
	LoadData(path string) error
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	Insert(n int, c Commit) error
	Delete(n int) error
	DeleteCurrent() error
	Index() (int, error)
	Pop() *Node
	Shift() *Node
	SearchUUID(uuID string) *Node
	Search(message string) *Node
	Reverse() *DoubleLinkedList
}

func QuickSort(arr []Commit) []Commit {
	if len(arr) < 2 {
		return arr
	}
	median := arr[rand.Intn(len(arr))].Date
	low, top, mid := make([]Commit, 0, len(arr)), make([]Commit, 0, len(arr)), make([]Commit, 0, len(arr))
	for _, elem := range arr {
		switch {
		case elem.Date.Sub(median) == 0:
			mid = append(mid, elem)
		case elem.Date.Sub(median) < 0:
			low = append(low, elem)
		case elem.Date.Sub(median) > 0:
			top = append(top, elem)
		}
	}
	low = QuickSort(low)
	top = QuickSort(top)
	low = append(low, mid...)
	low = append(low, top...)

	return low
}

func Decode(path string) ([]Commit, error) {
	c := make([]Commit, 0)
	o, _ := os.Open(path)
	dec := json.NewDecoder(o)
	err := dec.Decode(&c)
	if err != nil {
		return nil, errors.New("decode err")
	}
	return c, nil
}

func main() {
	dll := &DoubleLinkedList{}
	err := dll.LoadData("module3/task/test.json")
	if err != nil {
		fmt.Printf("%s", err)
	}
}

// LoadData загрузка данных из подготовленного json файла
func (d *DoubleLinkedList) LoadData(path string) error {
	// отсортировать список используя самописный QuickSort
	c, err := Decode(path)
	if err != nil {
		fmt.Println(err)
	}
	s := QuickSort(c)
	fmt.Println(s)
	return nil
}

// Len получение длины списка
func (d *DoubleLinkedList) Len() int {
	return d.len
}

// Current получение текущего элемента
func (d *DoubleLinkedList) Current() *Node {
	if d.curr == nil {
		return nil
	}
	return d.curr
}

// Next получение следующего элемента
func (d *DoubleLinkedList) Next() *Node {
	if d.curr == nil || d.curr.next == nil {
		return nil
	}
	return d.curr.next
}

// Prev получение предыдущего элемента
func (d *DoubleLinkedList) Prev() *Node {
	if d.curr == nil || d.curr.prev == nil {
		return nil
	}
	return d.curr.prev
}

// Insert вставка элемента после n элемента
func (d *DoubleLinkedList) Insert(n int, c Commit) error {
	node := &Node{data: &c}
	if n < 0 || n > d.len {
		return errors.New("out of range")
	}
	switch {
	case n == 0:
		if d.head == nil {
			d.head, d.tail = node, node
		} else {
			node.next = d.head
			d.head.prev = node
			d.head = node
		}
	case d.len == n:
		node.prev = d.tail
		d.tail.next = node
		d.tail = node
	default:
		curr := d.head
		for i := 0; i < n-1; i++ {
			curr = curr.next
		}
		node.prev = curr
		node.next = curr.next
		curr.next.prev = node
		curr.next = node
	}
	d.len++
	return nil
}

// Delete удаление n элемента
func (d *DoubleLinkedList) Delete(n int) error {
	if n <= 0 || n > d.len {
		return errors.New("out of range")
	}
	if n == 1 && d.head.next != nil {
		d.head = d.head.next
		d.head.prev = nil
	} else if n == 1 && d.head.next == nil {
		d.head, d.tail = nil, nil
	} else if n == d.len && n != 1 {
		d.tail = d.tail.prev
		d.tail.next = nil
	} else {
		curr := d.head
		for i := 0; i < n-1; i++ {
			curr = curr.next
		}
		curr.next.prev = curr.prev
		curr.prev.next = curr.next
	}
	d.len--
	return nil
}

// DeleteCurrent удаление текущего элемента
func (d *DoubleLinkedList) DeleteCurrent() error {
	if d.curr == nil {
		return errors.New("cant delete curr")
	}
	if d.curr.data.Message == d.head.data.Message {
		d.Shift()
		return nil
	}
	if d.curr.data.Message == d.tail.data.Message {
		d.Pop()
		return nil
	}
	d.curr.prev.next = d.curr.next
	d.curr.next.prev = d.curr.prev
	d.curr = d.curr.next
	d.len--
	return nil
}

// Index получение индекса текущего элемента
func (d *DoubleLinkedList) Index() (int, error) {
	if d.curr == nil {
		return -1, errors.New("current element is nil")
	}
	curr := d.head
	index := 0
	for curr != nil {
		if curr.data.Message == d.curr.data.Message {
			return index, nil
		}
		curr = curr.next
		index++
	}
	return -1, errors.New("current element not found in list")
}

// Pop Операция Pop
func (d *DoubleLinkedList) Pop() *Node {
	if d.tail == nil {
		return nil
	}
	val := d.tail
	d.tail = d.tail.prev
	if d.tail == nil {
		d.head = nil
	} else {
		d.tail.next = nil
	}
	d.len--
	return val
}

// Shift операция shift
func (d *DoubleLinkedList) Shift() *Node {
	if d.head == nil {
		return nil
	}
	val := d.head
	d.head = d.head.next
	if d.head == nil {
		d.tail = nil
	} else {
		d.head.prev = nil
	}
	d.len--
	return val
}

// SearchUUID поиск коммита по uuid
func (d *DoubleLinkedList) SearchUUID(uuID string) *Node {
	curr := d.head
	for curr != nil {
		if strings.Contains(fmt.Sprintf("%v", curr.data.UUID), uuID) {
			return curr
		}
		curr = curr.next
	}
	return nil
}

// Search поиск коммита по message
func (d *DoubleLinkedList) Search(message string) *Node {
	curr := d.head
	for curr != nil {
		if strings.Contains(fmt.Sprintf("%v", curr.data.Message), message) {
			return curr
		}
		curr = curr.next
	}
	return nil
}

// Reverse возвращает перевернутый список
func (d *DoubleLinkedList) Reverse() *DoubleLinkedList {
	newDll := &DoubleLinkedList{}
	curr := d.tail
	for curr != nil {
		newDll.AddToTail(*curr.data)
		curr = curr.prev
	}
	return newDll
}

func (d *DoubleLinkedList) AddToTail(c Commit) {
	node := &Node{data: &c}
	if d.head == nil {
		d.head = node
		d.tail = node
	} else {
		node.prev = d.tail
		d.tail.next = node
		d.tail = node
	}
}

type Node struct {
	data *Commit
	prev *Node
	next *Node
}

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

func GenerateJSON() {

	// Дополнительное задание написать генератор данных
	// используя библиотеку gofakeit

	filename := "module3/task/generate.json"
	err := checkFile(filename)
	if err != nil {
		log.Fatal(err)
	}

	data := make([]Commit, rand.Intn(10000))

	gofakeit.Seed(0)

	for i := range data {

		commit := Commit{
			Message: gofakeit.Street(),
			UUID:    gofakeit.UUID(),
			Date:    gofakeit.Date(),
		}
		data[i] = commit
	}

	dataBytes, err := json.Marshal(data)
	if err != nil {
		log.Fatal(err)
	}

	err = os.WriteFile(filename, dataBytes, 0644)
	if err != nil {
		log.Fatal(err)
	}

}

func checkFile(filename string) error {
	_, err := os.Stat(filename)
	if os.IsNotExist(err) {
		_, err = os.Create(filename)
		if err != nil {
			log.Fatal(err)
		}
	}
	return nil
}
